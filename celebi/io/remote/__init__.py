# -*- eval: (venv-workon "celebi"); -*-

from .monitor import RemoteMonitorWSGI

__all__ = ['RemoteMonitorWSGI']
